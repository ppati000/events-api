class ApiKeySerializer < ActiveModel::Serializer
  attributes :id, :access_token #not :scope, :expired_at, :created_at
  has_one :user, embed: :id #not sure what embed does yolo
end
