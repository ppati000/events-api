class UsersController < ApplicationController
  #calls shit from ApplicationController
  before_filter :ensure_authenticated, only: [:index]
  
  def index
    render json: { users: User.all }
  end
  
  def show
    render json: { user: User.find(params[:id]) }
  end
  
  def create
    user = User.create(user_params)
    if user.new_record? #if it wasn't saved duh
      render json: { errors: user.errors.messages }, status: 422
    else
      render json: { api_key: user.session_api_key }, status: 201
    end
  end
  
  private
  
    def user_params
      params.require(:user).permit(:name, :username, :email, :password,
            :password_confirmation)
    end
  
  
end
