class ApplicationController < ActionController::API
  
  #401 unless authorized
  def ensure_authenticated
    head :unauthorized unless current_user
  end
  
  def current_user
    api_key = ApiKey.active.where(access_token: token).first
    if api_key
      return api_key.user
    else
      return nil
    end
  end
  
  
  def token
    bearer = request.headers["HTTP_AUTHORIZATION"]
    
    #for tests
    bearer ||= request.headers["rack.session"].try(:[], 'Authorization')
    
    if bearer.present? #not blank
      return bearer.split.last #splits using whitespace, takes last
    else
      return nil
    end
  end
  
end
