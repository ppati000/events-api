require 'test_helper'

class SessionControllerTest < ActionController::TestCase
  
  def setup
    @pw ="12345678"
    @larry = User.create!(username: 'larry', email: 'larry@yolo.me',
          name: 'Larry Moulders', password: @pw, password_confirmation: @pw)
  end
  
  test "authenticate with username" do
    post 'create', { username_or_email: @larry.username, password: @pw }
    results = JSON.parse(response.body)
    assert results['api_key']['access_token'] =~ /\S{32}/
    assert results['api_key']['user_id'] == @larry.id
  end
  
  test "authenticate with email" do
    post 'create', { username_or_email: @larry.email, password: @pw }
    results = JSON.parse(response.body)
    assert results['api_key']['access_token'] =~ /\S{32}/
    assert results['api_key']['user_id'] == @larry.id
  end
  
  test "authenticate with invalid info" do
    post 'create', { username_or_email: @larry.email, password: "H4xx0r" }
    assert response.status == 401
  end
  
end
